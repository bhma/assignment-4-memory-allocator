#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

int passed_tests = 0;

#define RUN(test)                                                      \
    {                                                                  \
        printf("Testing: %s\n", #test);                                \
        heap_init(0);                                                  \
        char* warning = test();                                        \
        if (!warning) {                                                \
            printf("%s -- SUCCESS\n", #test);                          \
            passed_tests++;                                            \
        } else {                                                       \
            printf("%s -- FAIL: %s\n", #test, warning);                \
        }                                                              \
    }

static char *test_malloc() {
    void *mllc = _malloc(32);
    if (mllc == NULL) {
        return "no memory allocated";
    }
    return NULL;
}

static char *test_one_block_free() {
    void *frst = _malloc(0);
    void *scnd = _malloc(0);
    _free(scnd);
    struct block_header *block_frst = (struct block_header *) (frst - offsetof(struct block_header, contents));
    struct block_header *block_scnd = (struct block_header *) (scnd - offsetof(struct block_header, contents));
    if (!block_scnd->is_free) {
        return "the block hasn't been freed";
    }
    if (block_frst->next != block_scnd) {
        return "'next' reference doesn't match";
    }
    return NULL;
}

static char *test_two_blocks_free() {
    void *frst = _malloc(0);
    void *scnd = _malloc(0);
    _free(frst);
    _free(scnd);
    struct block_header *block_frst = (struct block_header *) (frst - offsetof(struct block_header, contents));
    struct block_header *block_scnd = (struct block_header *) (scnd - offsetof(struct block_header, contents));
    if (!block_scnd->is_free || !block_frst->is_free) {
        return "blocks haven't been freed";
    }
    if (block_frst->next != block_scnd) {
        return "'next' reference doesn't match";
    }
    return NULL;
}

static char *test_regions_expansion() {
    _malloc(8000);
    struct block_header *start = HEAP_START;
    struct block_header *expansion = (struct block_header *) (_malloc(8000) - offsetof(struct block_header, contents));
    if (start->next != expansion) {
        return "new region doesn't extend previous one";
    }
    if (start->capacity.bytes != 8000 || expansion->capacity.bytes != 8000) {
        return "wrong capacity";
    }
    return NULL;
}

static char *test_regions_expansion_other_place() {
    struct block_header *start = HEAP_START;
    void *prvt = mmap(HEAP_START + REGION_MIN_SIZE, 100, 0, MAP_PRIVATE | 0x20, -1, 0);
    _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header *expansion = (struct block_header *) (
            _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents))
            - offsetof(struct block_header, contents));
    munmap(prvt, 100);
    if (start->next != expansion) {
        return "wrong reference to a new region";
    }
    if (start + offsetof(struct block_header, contents) + start->capacity.bytes == expansion) {
        return "wrong location of a new region";
    }
    return NULL;
}

int main() {
    RUN(test_malloc);
    RUN(test_one_block_free);
    RUN(test_two_blocks_free);
    RUN(test_regions_expansion);
    RUN(test_regions_expansion_other_place);
    printf("-----\n");
    printf("Passed %d tests out of 5\n", passed_tests);
    return 0;
}